import {  obtenerRobots } from './arreglos';


describe ('Pruebas de arreglos', () =>{
    it('Debe retornar al menos tres robots', () => {
      const res = obtenerRobots();
      expect(res.length).toBeGreaterThanOrEqual(3);
    });

    it('Debe retornar a Megaman y a Ultron', () => {
      const res = obtenerRobots();
      expect(res).toContain('Megaman');
      expect(res).toContain('Ultron');
    });
});
