import { mensaje } from './string';


describe('Pruebas de string', () => {
    it('Debe regresar un string', () => {
      const respuesta = mensaje ('Esteban');

      expect( typeof respuesta).toBe('string');
    });

    it('Debe regresar un saludo con el nombre enviado', () => {
      const nombre = 'Esteban';
      const respuesta = mensaje (nombre);

      expect( respuesta).toContain(nombre);
    });
});
