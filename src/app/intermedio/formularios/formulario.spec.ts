import { FormularioResgister } from './formulario';
import { FormBuilder } from "@angular/forms";


describe('Formularios', () => {

  let componente: FormularioResgister;

  beforeEach(() => {
    componente = new FormularioResgister(new FormBuilder());
  });

  it('Debe de crear un formulario con dos campos', () => {

    expect(componente.form.contains('email')).toBeTruthy();
    expect(componente.form.contains('password')).toBeTruthy();
  });

  it('El email debe ser obligatorio', () => {
    const control = componente.form.get('email');
    control.setValue('');
    expect(control.valid).toBeFalsy();
  });

});
